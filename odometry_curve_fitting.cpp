/*
Refs:
1. http://ro.uow.edu.au/cgi/viewcontent.cgi?article=1101&context=infopapers
2. https://pdfs.semanticscholar.org/407f/4ffc6b8ef4e80406aa77c18b0099c42bd968.pdf
3. https://www.cs.princeton.edu/courses/archive/fall11/cos495/COS495-Lecture5-Odometry.pdf
*/

#include <vector>
#include <fstream>
#include <iostream>
#include <Eigen/Eigen>
#include "ceres/ceres.h"
#include "glog/logging.h"

using namespace ceres;
using namespace std;

struct OdometryResidual
{
    OdometryResidual(Eigen::Vector3d x, Eigen::Vector3d y)
    : x_(x), y_(y){}

    template <typename T>
    bool operator()(const T* const k33, T* residual) const  // 9 parameters, 1 outputs
    {
        // calculate residual
        residual[0] = T(y_[0]) - (k33[0]*T(x_[0]) + k33[1]*T(x_[1]) + k33[2]*T(x_[2]));
        residual[1] = T(y_[1]) - (k33[3]*T(x_[0]) + k33[4]*T(x_[1]) + k33[5]*T(x_[2]));
        residual[2] = T(y_[2]) - (k33[6]*T(x_[0]) + k33[7]*T(x_[1]) + k33[8]*T(x_[2]));
        return true;
    }

    private:
    Eigen::Vector3d x_;
    Eigen::Vector3d y_;
};

void loadData(vector<Eigen::Vector3d>& odoPose, vector<Eigen::Vector3d>& truePose)
{
    ifstream fin_data("../data/calib.dat");
    if(!fin_data)
    {
        cerr << "can't open or find calib.dat" << endl;
        return;
    }
    
    double x, y, theta;
    double poseRaw[3] = {0};
    double poseTrue[3] = {0};

    // keep computed trajectory
    vector<Eigen::Vector3d> odometry_true_trajectory, odometry_raw_trajectory;

    Eigen::Matrix3d odometry_raw_pose = Eigen::Matrix3d::Identity();        // init pose, x=y=0, theta = 0
    Eigen::Matrix3d odometry_true_pose = Eigen::Matrix3d::Identity();        // init pose, x=y=0, theta = 0
    while(!fin_data.eof())
    {
        // true odometry data
        fin_data >> x;
        fin_data >> y;
        fin_data >> theta;
        
        // Odometry Kinematic Model
        // update odometry pose
        Eigen::Matrix3d delta_trans_matrix;

        delta_trans_matrix << ceres::cos(theta), -ceres::sin(theta), x, ceres::sin(theta), ceres::cos(theta), y, 0, 0, 1;
        odometry_true_pose = odometry_true_pose * delta_trans_matrix;
        double theta_update = ceres::atan2(odometry_true_pose(1,0), odometry_true_pose(0,0));
        odometry_true_trajectory.push_back(Eigen::Vector3d(odometry_true_pose(0,2), odometry_true_pose(1,2), theta_update));
        truePose.push_back(Eigen::Vector3d(x,y,theta));

        // raw odometry data
        fin_data >> x;
        fin_data >> y;
        fin_data >> theta;

        // Odometry Kinematic Model
        // update odometry pose
        delta_trans_matrix << ceres::cos(theta), -ceres::sin(theta), x, ceres::sin(theta), ceres::cos(theta), y, 0, 0, 1;
        odometry_raw_pose = odometry_raw_pose * delta_trans_matrix;
        theta_update = ceres::atan2(odometry_raw_pose(1,0), odometry_raw_pose(0,0));
        odometry_raw_trajectory.push_back(Eigen::Vector3d(odometry_raw_pose(0,2), odometry_raw_pose(1,2), theta_update));
        odoPose.push_back(Eigen::Vector3d(x,y,theta));

        // This is import data to matlab
        // for (int i=0; i<odoPose.size(); i++)
        // {
        //     cout << odometry_raw_trajectory[i][0] << " " << odometry_raw_trajectory[i][1] << " " << odometry_raw_trajectory[i][2] << " ";
        //     cout << odometry_true_trajectory[i][0] << " " << odometry_true_trajectory[i][1] << " " << odometry_true_trajectory[i][2] << endl;
        // }
    }
}

int main(int argc, char** argv)
{
    google::InitGoogleLogging(argv[0]);

    // parameter for estimating
    double k33[9] = {1.0, 0.0, 0.0, 0.0, 1.0, 0.0, 0.0, 0.0, 1.0};
    
    // load data
    vector<Eigen::Vector3d> odoPose;
    vector<Eigen::Vector3d> truePose;
    loadData(odoPose, truePose);

    // Add residual blocks
    Problem problem;
    for(int i=0; i<odoPose.size(); i++)
    {
        problem.AddResidualBlock(
            new AutoDiffCostFunction<OdometryResidual, 3, 9>(
                new OdometryResidual(odoPose[i], truePose[i])),
                new ceres::CauchyLoss(0.5),
                k33
            );
    }

    Solver::Options options;
    options.max_num_iterations = 1000;
    options.linear_solver_type = ceres::DENSE_QR;
    options.minimizer_progress_to_stdout = true;

    Solver::Summary summary;
    Solve(options, &problem, &summary);
    cout << summary.BriefReport() << endl;

    // ouput estimated parameters
    cout << k33[0] << " " << k33[1] << " " << k33[2] << endl;
    cout << k33[3] << " " << k33[4] << " " << k33[5] << endl;
    cout << k33[6] << " " << k33[7] << " " << k33[8] << endl;

    // Calcualte Odometry Calibration data
    // Eigen::Matrix3d estimated_matrix;
    // estimated_matrix << k33[0], k33[1], k33[2],
    //                     k33[3], k33[4], k33[5],
    //                     k33[6], k33[7], k33[8];

    // cout << "Odometry estimated pose." << endl << endl;
    // Eigen::Matrix3d odometry_est_pose = Eigen::Matrix3d::Identity();
    // for(int i=0; i<odoPose.size(); i++)
    // {
    //     // Odometry Kinematic Model
    //     // update odometry pose
    //     Eigen::Vector3d odo_est = estimated_matrix * odoPose[i];        // using odometry calibration paramters
    //     Eigen::Matrix3d delta_trans_matrix;
    //     delta_trans_matrix << ceres::cos(odo_est[2]), -ceres::sin(odo_est[2]), odo_est[0], ceres::sin(odo_est[2]), ceres::cos(odo_est[2]), odo_est[1], 0, 0, 1;
    //     odometry_est_pose = odometry_est_pose * delta_trans_matrix;
    //     double theta_update = ceres::atan2(odometry_est_pose(1,0), odometry_est_pose(0,0));
    //     cout << odometry_est_pose(0,2) << " " << odometry_est_pose(1,2) << " " << theta_update << endl; 
    // }
    return 0;
}
