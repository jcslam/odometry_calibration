#include <iostream>
#include <vector>
#include <fstream>

using namespace std;

struct OdometryPose
{
    OdometryPose(double xx=0.0, double yy=0.0, double a=0.0)
    : x(xx), y(yy), theta(a) {}
    double x;
    double y;
    double theta;
};

int main(int argc, char** argv)
{

    ifstream fin_data("../data/calib.dat");
    if(!fin_data)
    {
        cerr << "can't open or find calib.dat" << endl;
        return 1;
    }
    vector<OdometryPose> odoPose;
    vector<OdometryPose> truePose;

    double x, y, theta;
    while(!fin_data.eof())
    {
        fin_data >> x;
        fin_data >> y;
        fin_data >> theta;
        odoPose.push_back(OdometryPose(x,y,theta));
        fin_data >> x;
        fin_data >> y;
        fin_data >> theta;
        truePose.push_back(OdometryPose(x,y,theta));
    }

    // for (int i=0; i<odoPose.size(); i++)
    // {
    //     cout << odoPose[i].x << " " << odoPose[i].y << " " << odoPose[i].theta << " ";
    //     cout << truePose[i].x << " " << truePose[i].y << " " << truePose[i].theta << endl;
    // }

    int a[3][3] = {1,2,3,4,5,6,7,8,9};
    cout << a[0][0] << a[0][1] << a[0][2] << endl;
    return 0;
}